package com.example.fragmenttask

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

class ButtonsFragment : Fragment() {
    private lateinit var listener1 : ColorListener
    private lateinit var listener2: ColorListener
    private lateinit var listener3 : Context

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener3 = context
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_buttons, container, false)
    }

    fun initializeListeners() {
        val changeColor : Button = requireActivity().findViewById(R.id.change_color)!!
        val swapFragments : Button = requireActivity().findViewById(R.id.swap_fragment)!!
        val manager = requireActivity().supportFragmentManager
        listener1 = manager.findFragmentById(R.id.fragment2) as ColorListener
        listener2 = manager.findFragmentById(R.id.fragment3) as ColorListener

        changeColor.setOnClickListener{
            val color1 = ((listener1 as Fragment).view?.background as ColorDrawable).color
            val color2 = ((listener2 as Fragment).view?.background as ColorDrawable).color
            listener1.receiveColor(color2)
            listener2.receiveColor(color1)
            
            if (listener1 is Fragment3) {
                (listener3 as ColorsListener).receiveColors(color1, color2)
            } else {
                (listener3 as ColorsListener).receiveColors(color2, color1)
            }
        }

        swapFragments.setOnClickListener { (listener3 as SwapFragments).swapFragments() }
    }

    interface ColorListener {
        fun receiveColor(color1 : Int)
    }

    interface ColorsListener {
        fun receiveColors(color1 : Int, color2: Int)
    }

    interface SwapFragments {
        fun swapFragments()
    }
}