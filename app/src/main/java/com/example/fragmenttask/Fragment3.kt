package com.example.fragmenttask

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class Fragment3(val color: Int = 0) : Fragment(), ButtonsFragment.ColorListener{
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_3, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        savedInstanceState?.apply {
            receiveColor(savedInstanceState.getInt("color"))
        }

        receiveColor(color)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("color", (view?.background as? ColorDrawable)?.color ?: 0)
    }

    override fun receiveColor(color1: Int) {
        view?.setBackgroundColor(color1)
    }
}