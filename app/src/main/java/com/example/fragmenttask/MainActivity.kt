package com.example.fragmenttask

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment

class MainActivity : AppCompatActivity(), ButtonsFragment.SwapFragments, ButtonsFragment.ColorsListener{
    lateinit var frag1 : Fragment
    lateinit var frag2 : Fragment
    lateinit var buttons : Fragment
    var color1 : Int = 0
    var color2 : Int = 0
    var swapped = false
    var initialized = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fun addFragments(frag1 : Fragment, frag2 : Fragment) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment2, frag1)
                .replace(R.id.fragment3, frag2)
                .commit()
        }

        savedInstanceState?.apply {
            swapped = getBoolean("swapped")
            color1 = getInt("color1")
            color2 = getInt("color2")
            initialized = getBoolean("initialized")
        }

        if (!swapped) addFragments(Fragment2(color1), Fragment3(color2))
        else addFragments(Fragment3(color2), Fragment2(color1))


        buttons = supportFragmentManager.findFragmentById(R.id.buttons)!!
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        frag1 = supportFragmentManager.findFragmentById(R.id.fragment2)!!
        frag2 = supportFragmentManager.findFragmentById(R.id.fragment3)!!

        if (!initialized) {
            color1 = GenerateColor().generateRandomColor()
            color2 = GenerateColor().generateRandomColor()
        }

        if (swapped) {
            frag1.view?.setBackgroundColor(color2)
            frag2.view?.setBackgroundColor(color1)
        } else {
            frag1.view?.setBackgroundColor(color1)
            frag2.view?.setBackgroundColor(color2)
        }

        (buttons as ButtonsFragment).initializeListeners()
    }

    override fun swapFragments() {
        val newFrag1 = Fragment2(color1)
        val newFrag2 = Fragment3(color2)

        if (swapped) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment2, newFrag1)
                .replace(R.id.fragment3, newFrag2)
                .runOnCommit {
                    (buttons as ButtonsFragment).initializeListeners()
                    frag1 = newFrag2
                    frag2 = newFrag1
                    swapped = false
                }.commit()
        } else {
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment2, newFrag2)
                .replace(R.id.fragment3, newFrag1)
                .runOnCommit {
                    (buttons as ButtonsFragment).initializeListeners()
                    frag1 = newFrag1
                    frag2 = newFrag2
                    swapped = true
                }.commit()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putBoolean("swapped", swapped)
        outState.putInt("color1", color1)
        outState.putInt("color2", color2)
        outState.putBoolean("initialized", true)
    }

    override fun receiveColors(c1: Int, c2: Int) {
        color1 = c1
        color2 = c2
    }
}